import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import unit.CavalryUnit;
import unit.CommanderUnit;
import unit.InfantryUnit;
import unit.RangedUnit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test class for battle class
 */
class BattleTest {

    Army humans, orcs;
    Battle battle;

    /**
     * test to check if simulate() works and if it returns a winner after simulating
     * making two armies and using a for loop to add all the units
     * then creating a battle
     */
    @Test
    void testSimulateReturnAWinner() {
         humans = new Army("Human army");
         orcs = new Army("Orcish horde");


         for(int i = 0; i<500;i++){
             humans.add(new InfantryUnit("Footman", 100));
             orcs.add(new InfantryUnit("Grunt", 100));
         }

        for(int i = 0; i<100;i++){
            humans.add(new CavalryUnit("Knight", 100));
            orcs.add(new CavalryUnit("Raider", 100));
        }
        for(int i = 0; i<200;i++){
            humans.add(new RangedUnit("Archer", 100));
            orcs.add(new RangedUnit("Spearman", 100));
        }

        humans.add(new CommanderUnit(" Mountain King", 100));
        orcs.add(new CommanderUnit("Gul´dan", 180));
        battle = new Battle( humans, orcs);

        assertTrue(battle.simulate().equals(humans)||battle.simulate().equals(orcs));
        assertFalse(battle.simulate().equals(humans)&&battle.simulate().equals(orcs));

    }

}