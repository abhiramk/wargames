package unit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test class for unit class
 */
class UnitTest {
    Unit unit;

    /**
     * method to create an unit for each test
     */
    @BeforeEach
    public void reset(){
        unit = new CavalryUnit("Archer", 56);
    }

    /**
     * test to check if getName() return correct name
     */
    @Test
    void testIfGetNameReturnsCorrectName() {

        assertEquals("Archer", unit.getName());
    }

    /**
     * test to check if getHealth() returns correct health
     */
    @Test
    void TestIfGetHealthReturnsCorrectHealth() {
        assertEquals(56, unit.getHealth());

    }

    /**
     * test to check if getAttack() return correct attack
     */
    @Test
    void TestIfGetAttackReturnsCorrectAttack() {
        assertEquals(20, unit.getAttack());
    }

    /**
     * test to check if getArmor return correct armor
     */
    @Test
    void TestOfGetArmorReturnsCorrectArmor() {
        assertEquals(12, unit.getArmor());
    }

    /**
     * test to check if setHealth sets the correct health
     */
    @Test
    void TestIfSetHealthSetsCorrectHealth() {

        unit.setHealth(45);

        assertEquals(45, unit.getHealth());

    }

    /**
     * test to check if attack() sets correct health
     */
    @Test
    void testIfAttackReturnsCorrectHealth() {
        CavalryUnit unit = new CavalryUnit("Archer", 55);
        Unit unitOpponent = new CavalryUnit("Unit", 40);

        unit.attack(unitOpponent);

        assertEquals(unitOpponent.getHealth(),27);

        unit.attack(unitOpponent);

        assertEquals(unitOpponent.getHealth(),18);
    }
    /**
     * test to check it is allowed to set name blank
     */
    @Test
    void testIfBlankNameIsNotAllowed() {
        try {
            RangedUnit unit = new RangedUnit("", 45);

            fail();
        } catch (IllegalArgumentException name) {
            assertEquals("Name can not be blank", name.getMessage());
        }
    }

    /**
     * test to check if it is allowed to set negative health
     */
    @Test
    void testIfHealthCanBeNegative(){
        try {
            RangedUnit unit = new RangedUnit("Archer", -2);

            fail();
        } catch (IllegalArgumentException health){
            assertEquals("Health can not be less than 0", health.getMessage());
        }
    }

    /**
     * test to check if it is allowed to set negative armor
     */
    @Test
    void testIfArmorCanBeNegative(){
        try {
            RangedUnit unit = new RangedUnit("Archer", 4, 20, -7);

            fail();
        } catch (IllegalArgumentException armor){
            assertEquals("Armor can not be less than 0", armor.getMessage());
        }
    }
}