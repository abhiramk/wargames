import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import unit.CavalryUnit;
import unit.Unit;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test class for army class
 */
class ArmyTest {
    Army army;
    Unit unit1, unit2;

    @BeforeEach
    public void reset(){
       army = new Army("Army");
       unit1 = new CavalryUnit("mtTestUnit", 30);
       unit2 = new CavalryUnit("testUnitCavalary", 45);

    }

    /**
     * test to check if the name returned using the method getName() is correct
     */
    @Test
    void testIfGetNameReturnCorrectName() {
        String name = "Army";
        Army army = new Army(name, new ArrayList<>());

        assertEquals(name, army.getName());
    }

    /**
     * test to check if the method add() adds an unit to the desired army class
     */
    @Test
    void testIfAddWorks() {
        army.add(unit1);

        assertTrue(army.hasUnits());
    }

    /**
     * test to check if the method addAll() works
     */
    @Test
    void testIfAddAllWorks() {
        ArrayList<Unit> units = new ArrayList<>();
        assertEquals(army.getAllUnits().size(), 0);
        units.add(unit1);
        units.add(unit2);
        army.addAll(units);
        assertEquals(army.getAllUnits().size(), 2);

    }

    /**
     * test to check if the method remove() removes desired unit from the army class
     */
    @Test
    void testIfRemoveWorks() {
        army.add(unit1);
        assertEquals(army.getAllUnits().size(), 1 );
        army.remove(unit1);
        assertFalse(army.hasUnits());

    }

    /**
     * test to check if the method hasUnit() returns true when it has units and false if not
     */
    @Test
    void testIfHasUnitsChecksIfArmyContainsUnits() {
        assertFalse(army.hasUnits());
        //trenger ikke denne, men har den med som en ekstra sjekk
        assertEquals(army.getAllUnits().size(), 0);

        army.add(unit1);

        assertTrue(army.hasUnits());
    }

    /**
     * test to check if getAllUnits() returns all units in the army class
     */
    @Test
    void testIfGetAllUnitsReturnsAllUnits() {
        ArrayList<Unit> units = new ArrayList<>();
        units.add(unit1);
        units.add(unit2);

        army.addAll(units);

        assertEquals(army.getAllUnits().size(), 2);

    }

}