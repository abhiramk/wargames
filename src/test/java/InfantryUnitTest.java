import org.junit.jupiter.api.Test;
import unit.CavalryUnit;
import unit.InfantryUnit;
import unit.Unit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test class for InfantryUnit class
 */
class InfantryUnitTest {

    /**
     * test to check if getAttackBonus() return the correct bonus after each attack
     */
    @Test
    void testIfGetAttackBonusReturnsCorrectBonus() {
        InfantryUnit unit = new InfantryUnit("Swordsman", 3);

        int attackBonus = unit.getAttackBonus();

        assertEquals(2, attackBonus);

    }

    /**
     * test if getResistBonus() returns the correct bonus after each attack
     */
    @Test
    void testIfGetResistBonusReturnsCorrectResist() {
        InfantryUnit unit = new InfantryUnit("Archerman", 5);

        int resistBonus = unit.getResistBonus();

        assertEquals(1, resistBonus);
    }
    /**
     * test to check if attack() sets correct health
     */
    @Test
    void testIfAttackReturnsCorrectHealth() {
        InfantryUnit unit = new InfantryUnit("Archer", 55);
        Unit unitOpponent = new InfantryUnit("Unit", 40);

        unit.attack(unitOpponent);

        assertEquals(unitOpponent.getHealth(),34);
    }

}
