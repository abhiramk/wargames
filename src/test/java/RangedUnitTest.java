
import org.junit.jupiter.api.Test;
import unit.CavalryUnit;
import unit.RangedUnit;
import unit.Unit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test class for RangedUnit class
 */
class RangedUnitTest {

    /**
     * test to check if getAttackBonus() return the correct bonus after each attack
     */
    @Test
    void testIfGetAttackBonusReturnsCorrectBonus() {
        RangedUnit unit = new RangedUnit("Archer", 2);

       int attackBonus = unit.getAttackBonus();

       assertEquals(3, attackBonus);
    }
    /**
     * test if getResistBonus() returns the correct bonus after each attack
     */
    @Test
    void testIfGetResistBonusReturnsCorrectResist() {
        RangedUnit unit = new RangedUnit("5", 2);

        int resistBonus = unit.getResistBonus();

        assertEquals(6, resistBonus);

        resistBonus = unit.getResistBonus();

        assertEquals(4, resistBonus);

        resistBonus = unit.getResistBonus();

        assertEquals(2, resistBonus);

        resistBonus = unit.getResistBonus();

        assertEquals(2, resistBonus);

    }
    /**
     * test to check if attack() sets correct health
     */
    @Test
    void testIfAttackReturnsCorrectHealth() {
        RangedUnit unit = new RangedUnit("Archer", 55);
        Unit unitOpponent = new RangedUnit("Unit", 40);

        unit.attack(unitOpponent);

        assertEquals(unitOpponent.getHealth(),36);

        unit.attack(unitOpponent);

        assertEquals(unitOpponent.getHealth(),30);

        unit.attack(unitOpponent);

        assertEquals(unitOpponent.getHealth(),22);
    }

}

