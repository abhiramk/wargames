import org.junit.jupiter.api.Test;
import unit.CavalryUnit;
import unit.Unit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test class for cavalryUnit class
 */
class CavalryUnitTest {

    /**
     * test to check if getAttackBonus() return the correct bonus after each attack
     */
    @Test
    void testIfGetAttackBonusReturnsCorrectBonus() {
        CavalryUnit unit = new CavalryUnit("archebold", 3);

        int attackBonus = unit.getAttackBonus();

        assertEquals(6, attackBonus);

        attackBonus = unit.getAttackBonus();

        assertEquals(2, attackBonus);
    }

    /**
     * test if getResistBonus() returns the correct bonus
     */
    @Test
    void testIfGetResistBonusReturnsCorrectResist() {
        CavalryUnit unit = new CavalryUnit("Archer", 6);

        int resistBonus = unit.getResistBonus();

        assertEquals(resistBonus, 1);
    }

}