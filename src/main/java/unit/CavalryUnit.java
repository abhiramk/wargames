package unit;

/**
 * CavalryUnit class
 */
public class CavalryUnit extends Unit {

    int numberOfAttacks = 0;

    /**
     * constructor with all four attributes
     *
     * @param name   short describing name
     * @param health a value that describes the units' health
     *               it reduces when the unit is attacked, but can never be less than 0
     * @param attack an attack value that describes the units weapon
     * @param armor  a defence value that protects under an attack
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * simplified constructor with set attack value to 20 and armor value to 12
     * @param name short describing name
     * @param health a value that describes the units' health
     *            it reduces when the unit is attacked, but can never be less than 0
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    /**
     * method to find out correct bonus depending on number of previous attacks
     * @return 6 first attack and 2 after that
     */
    @Override
    public int getAttackBonus() {
        numberOfAttacks++;
        if( numberOfAttacks == 1){
            return 6;
        }else{
            return 2;
        }
    }
    /**
     *  defence bonus
     * @return 1
     */
    @Override
    public int getResistBonus() {
        return 1;
    }
}
