package unit;

import unit.CavalryUnit;

/**
 * CommanderUnit class
 */
public class CommanderUnit extends CavalryUnit {

    /**
     * constructor with all four attributes
     *
     * @param name   short describing name
     * @param health a value that describes the units' health
     *               it reduces when the unit is attacked, but can never be less than 0
     * @param attack an attack value that describes the units weapon
     * @param armor  a defence value that protects under an attack
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * simplified constructor with set attack value to 25 and armor value to 15
     * @param name short describing name
     * @param health a value that describes the units' health
     *            it reduces when the unit is attacked, but can never be less than 0
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }




}
