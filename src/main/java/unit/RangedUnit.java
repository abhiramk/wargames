package unit;

/**
 * RangedUnit class
 */
public class RangedUnit extends Unit {
    int numberOfAttacks = 0;


    /**
     * constructor with all four attributes
     *
     * @param name   short describing name
     * @param health a value that describes the units' health
     *               it reduces when the unit is attacked, but can never be less than 0
     * @param attack an attack value that describes the units weapon
     * @param armor  a defence value that protects under an attack
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * simplified constructor with set attack value to 15 and armor value to 8
     * @param name short describing name
     * @param health a value that describes the units' health
     *            it reduces when the unit is attacked, but can never be less than 0
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);

    }
    /**
     * method to get the  attack bonus
     *unit has strength in close fights
     * @return 3
     */
    @Override
    public int getAttackBonus() {
        return 3;
    }

    /**
     * method to get resist bonus
     * @return 6 first attack, 4 second attack and 2 every attack after the third
     */
    @Override
    public int getResistBonus() {
        numberOfAttacks++;
        if (numberOfAttacks == 1) {
            return 6;
        }
        if (numberOfAttacks == 2) {
            return 4;
        } else {
            return 2;
        }

    }

}
