package unit;

/**
 * InfantryUnit class
 */
public class InfantryUnit extends Unit {
    /**
     * constructor with all four attributes
     *
     * @param name   short describing name
     * @param health a value that describes the units' health
     *               it reduces when the unit is attacked, but can never be less than 0
     * @param attack an attack value that describes the units weapon
     * @param armor  a defence value that protects under an attack
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * simplified constructor with set attack value to 15 and armor value to 10
     * @param name short describing name
     * @param health a value that describes the units' health
     *            it reduces when the unit is attacked, but can never be less than 0
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    /**
     *unit has strength in close fights
     * @return 2
     */
    @Override
    public int getAttackBonus() {
        return 2;
    }

    /**
     *  defence bonus
     * @return 1
     */
    @Override
    public int getResistBonus() {
        return 1;
    }
}
