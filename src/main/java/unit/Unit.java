package unit;

/**
 * Unit class
 */
public abstract class Unit {

    private String name;
    private int health;
    private int attack;
    private int armor;


    /**
     * constructor with all four attributes
     *
     * @param name   short describing name
     * @param health a value that describes the units' health
     *               it reduces when the unit is attacked, but can never be less than 0
     * @param attack an attack value that describes the units weapon
     * @param armor  a defence value that protects under an attack
     */
    public Unit(String name, int health, int attack, int armor) {
        this.name = name;
        if (name == "") {
            throw new IllegalArgumentException("Name can not be blank");
        }
        this.health = health;
        if (health < 0) {
            throw new IllegalArgumentException("Health can not be less than 0");
        }
        this.attack = attack;
        this.armor = armor;
        if (armor < 0) {
            throw new IllegalArgumentException("Armor can not be less than 0");
        }
    }

    /**
     * accessor method for the name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * accessor method for the health
     *
     * @return helath value
     */
    public int getHealth() {
        return health;
    }

    /**
     * accessor method for the attack
     *
     * @return attack value
     */
    public int getAttack() {
        return attack;
    }

    /**
     * accessor method for the armor
     *
     * @return armor value
     */
    public int getArmor() {
        return armor;
    }

    /**
     * mutator method for the health
     *
     * @param health value
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * abstract method, receive bonus when attacking another unit
     *
     * @return number value
     */
    public abstract int getAttackBonus();

    /**
     * method, receive bonus when defending from another unit
     *
     * @return number value
     */
    public abstract int getResistBonus();

    /**
     * to string method for unit
     *
     * @return returns information about the unit(name, health, attack and armor)
     */
    @Override
    public String toString() {
        return "name: " + name +
                " health: " + health +
                " attack: " + attack +
                " armor: " + armor;
    }

    /**
     * formula to calculate health of opponent after attack
     *
     * @param opponent opponent unit
     */
    public void attack(Unit opponent) {
        int health = opponent.health - (this.attack + this.getAttackBonus()) + (opponent.getArmor() + opponent.getResistBonus());
        opponent.setHealth(health);
    }


}


