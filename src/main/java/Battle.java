import unit.Unit;

/**
 * Battle class
 */
public class Battle {

    private Army armyOne;
    private Army armyTwo;

    /**
     * Constructor with two army classes as parameter
     * @param armyOne first army
     * @param armyTwo second army
     */
    public Battle(Army armyOne, Army armyTwo) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }

    /**
     * method to simulate a battle
     * @return the winner
     */
    public Army simulate(){
        while(armyTwo.hasUnits() && armyOne.hasUnits()) {
            Unit unitTwo = armyTwo.getRandom();
            armyOne.getRandom().attack(unitTwo);
            if (unitTwo.getHealth() <= 0) {
                armyTwo.remove(unitTwo);
            }
            if (!armyTwo.hasUnits()) {
                break;
            }
                Unit unitOne = armyOne.getRandom();
                armyTwo.getRandom().attack(unitOne);
                if (unitOne.getHealth() <= 0) {
                    armyOne.remove(unitOne);
                }
            }
        if(!armyTwo.hasUnits()) {
            return armyOne;
        } else {
            return armyTwo;
        }
    }

    /**
     * to-string method
     * @return text with each army
     */
    @Override
    public String toString() {
        return "Army number one: " + armyOne +
                " Army number two: " + armyTwo;
    }
}
