import unit.Unit;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.List;

public class Army {

    private final String name;
    private List<Unit> units;

    /**
     * constructor with only name
     * @param name name of army
     */
    public Army(String name) {
        this.name = name;
        this.units = new ArrayList<>();
    }

    /**
     * construvvtor for all attributes
     * @param name   name of army
     * @param units  list of all the units
     */
    public Army(String name, List<Unit> units) {
        this.name = name;
        this.units = units;
    }

    /**
     * get method for name
     * @return   name
     */
    public String getName() {
        return name;
    }

    /**
     * method to add a
     * @param unit unit object
     * @return true or false
     */
    public boolean add(Unit unit){
        units.add(unit);
        return true;
    }

    /**
     * add all method
     * @param units unit list
     * @return true or false
     */
    public boolean addAll(List<Unit> units){
        this.units.addAll(units);
        return true;
    }

    /**
     * method to remove unit from list
     * @param unit unit object
     * @return true or false
     */
    public boolean remove(Unit unit){
        units.remove(unit);
        return true;
    }

    /**
     * check if unit list is empty or not
     * @return true or false
     */
    public boolean hasUnits(){
        return !units.isEmpty();

    }

    /**
     * method for printing all units from unit list
     * @return list with all units
     */
     public List<Unit> getAllUnits() {
         return units;                    
}

    /**
     * method to find a random unit from list
     * @return random unit
     */
     public Unit getRandom(){
        Random rand = new Random();
        return units.get(rand.nextInt(units.size()));

     }

    /**
     * equals method
     * @param o object o
     * @return true or false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Army)) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    /**
     * hashcode
     * @return number
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }

    /**
     * to string method
     * @return name and units
     */
    @Override
    public String toString() {
        return "name: " + name +
                " units: " + units;
    }
}
